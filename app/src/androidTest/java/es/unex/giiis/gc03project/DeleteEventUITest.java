package es.unex.giiis.gc03project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import es.unex.giiis.gc03project.activities.MainActivity;
import es.unex.giiis.gc03project.objects.Evento;
import es.unex.giiis.gc03project.objects.User;
import es.unex.giiis.gc03project.room_db.TeamMatchDataBase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@RunWith(AndroidJUnit4.class)

public class DeleteEventUITest {

        @Rule
        public ActivityTestRule<MainActivity> mActivityRule =
                new ActivityTestRule<>(MainActivity.class);
        SharedPreferences.Editor preferencesEditor;
        Intent intent;
        Evento e;
        User u;
        TeamMatchDataBase evento_dataBase;
        long id_evento;

        @Before
        public void before() throws Exception {
                //take shared preferences, if necessary
                Context targetContext = getInstrumentation().getTargetContext();
                preferencesEditor = targetContext.getSharedPreferences("Preferences", Context.MODE_PRIVATE).edit();
                evento_dataBase = TeamMatchDataBase.getInstance(targetContext);
                //Borranos todos los eventos que haya en la BD
                evento_dataBase.getDao().deleteAllEventos();

                //nos creamos un evento que será el que editemos
                e =  new Evento("EventoTest1",
                        new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2020"), 1,
                        "TextoPrueba", Evento.Deporte.TENIS, "PISTA PRUEBA", 1,
                        "", "", "");

                //Nos creamos un usuario para poder realizar el test ya que require estar logueado
                u= new User("UserTest","UserTest@user.com","12345678","");
                evento_dataBase.getDao().insertUser(u);

                //Nos deslogueamos
                preferencesEditor.clear().apply();
                // Toast.makeText(getApplicationContext(), "Se ha cerrado la sesión", Toast.LENGTH_SHORT).show();
                //Ahora procedmos a loguearnos
                Login();
                onView(withId(R.id.ic_home)).perform(click());

                //insertamos en BD el evento
                id_evento = evento_dataBase.getDao().insertEvento(e);
                e.setId(id_evento);

                // para iniciar el Test desde HOME
                onView(withId(R.id.ic_home)).perform(click());
        }

        public void Login() {
                String user="UserTest";
                String pass="12345678";
                openContextualActionModeOverflowMenu();
                onView(withText(R.string.action_login)).perform(click());
                onView(withId(R.id.et_email_username)).perform(typeText(user), closeSoftKeyboard());
                onView(withId(R.id.et_password)).perform(typeText(pass), closeSoftKeyboard());
                onView(withId(R.id.btn_login)).perform(click());
        }

        @Test
        public void ShouldDeleteEventFromDatabase() throws InterruptedException {
                preferencesEditor.putLong("id", 1);
                preferencesEditor.putString("username", "UserTest");
                preferencesEditor.putString("email", "UserTest@user.com");
                preferencesEditor.putString("password", "12345678");
                preferencesEditor.commit();

                onView(withId(R.id.my_recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
                Thread.sleep(2000);
                onView(withId(R.id.btn_deleteEvent)).perform(click());
                Thread.sleep(2000);
                onView(withText("Aceptar")).perform(click());
                Thread.sleep(2000);
        }
}
