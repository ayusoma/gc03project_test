package es.unex.giiis.gc03project.network;

import es.unex.giiis.gc03project.objects.Pistas;

import retrofit2.Call;
import retrofit2.http.GET;

public interface OpenDataService {
    @GET("GetData?dataset=om:EspacioDeportivo&format=json")
    Call<Pistas> cogerPistas();
}