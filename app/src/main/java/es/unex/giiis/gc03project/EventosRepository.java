package es.unex.giiis.gc03project;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import es.unex.giiis.gc03project.objects.Evento;
import es.unex.giiis.gc03project.room_db.TeamMatchDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventosRepository {

    private static final String LOG_TAG = "";
    private static EventosRepository sInstance;
    private final TeamMatchDAO mTeamMatchDao;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<Long> userFilterLiveData = new MutableLiveData<>();

    public EventosRepository(TeamMatchDAO mTeamMatchDao) {
        this.mTeamMatchDao = mTeamMatchDao;
    }

    public synchronized static EventosRepository getInstance(TeamMatchDAO dao) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new EventosRepository(dao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public LiveData<List<Evento>> getCurrentEventos() {
        // Return LiveData from Room. Use Transformation to get owner
        //Ahora devolvemos una transformación.
        //Cogemos LiveData
        return mTeamMatchDao.getLiveDataAllEventos();
    }

    public void setUsername(final long username){
        userFilterLiveData.setValue(username);

    }

    /**
     * Database related operations
     **/

    public LiveData<List<Evento>> getCurrentEventosCreados() {
        return Transformations.switchMap(userFilterLiveData, mTeamMatchDao::getAllEventosByUserId_LiveData);
    }


    public LiveData<List<Evento>> getCurrentEventosParticipacion() {
        return Transformations.switchMap(userFilterLiveData, mTeamMatchDao::getAllParticipacionesByUser_LiveData);
    }

    public LiveData<List<Evento>> getCurrentParticipacion() {
        return Transformations.switchMap(userFilterLiveData, mTeamMatchDao::getAllParticipacionesByUser_LiveData);
    }
}
