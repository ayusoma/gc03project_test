package es.unex.giiis.gc03project.room_db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.gc03project.objects.Equipo;
import es.unex.giiis.gc03project.objects.Evento;
import es.unex.giiis.gc03project.objects.ParticipacionUserEvento;
import es.unex.giiis.gc03project.objects.Pista;
import es.unex.giiis.gc03project.objects.User;


@Database(entities = {User.class, Evento.class, ParticipacionUserEvento.class, Equipo.class, Pista.class}, version = 1)
public abstract class TeamMatchDataBase extends RoomDatabase {

    public static TeamMatchDataBase instance;

    public static TeamMatchDataBase getInstance(Context context){
        if(instance == null)
            instance = Room.databaseBuilder(context, TeamMatchDataBase.class, "TeamMatch.db").build();
        return instance;
    }

    public abstract TeamMatchDAO getDao();

}