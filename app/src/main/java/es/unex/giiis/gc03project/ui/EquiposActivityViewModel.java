package es.unex.giiis.gc03project.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.gc03project.EquiposRepository;
import es.unex.giiis.gc03project.objects.Equipo;

import java.util.List;

public class EquiposActivityViewModel extends ViewModel {


    private final EquiposRepository mRepository;
    private final LiveData<List<Equipo>> mEquipos;



    //Constructor
    public EquiposActivityViewModel(EquiposRepository repository){
        mRepository = repository;
        mEquipos = mRepository.getCurrentEventos();
    }

    //Peticion de Eventos a la BD
    public void onRefresh(){
        mRepository.getCurrentEventos();
    }

    //Devuelvo el LiveData de todos los equipos
    public LiveData<List<Equipo>> getmEquipos() {
        return mEquipos;
    }


}
