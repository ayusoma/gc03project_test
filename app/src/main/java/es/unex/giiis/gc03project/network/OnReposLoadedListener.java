package es.unex.giiis.gc03project.network;

import es.unex.giiis.gc03project.objects.Binding;

import java.util.List;

public interface OnReposLoadedListener {

    public void onReposLoaded(List<Binding> b);

}
