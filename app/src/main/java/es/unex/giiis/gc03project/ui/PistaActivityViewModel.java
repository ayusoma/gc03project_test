package es.unex.giiis.gc03project.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import es.unex.giiis.gc03project.PistasRepository;
import es.unex.giiis.gc03project.objects.Pista;

import java.util.List;

//MODEL PARA OBTENER LAS PISTAS DEL PISTA REPOSITY (SE ACTUALIZAN).

public class PistaActivityViewModel extends ViewModel {

    private final PistasRepository mRepository;
    private final LiveData<List<Pista>> mPistas;



    public PistaActivityViewModel(PistasRepository repository){
        mRepository = repository;
        mPistas = mRepository.getCurrentPistas();
    }

    public void onRefresh(){
        mRepository.doFetchPistas();
    }

    public LiveData<List<Pista>> getPistas() {
        return mPistas;
    }


}
